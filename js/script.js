// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.querySelectorAll("p");

for (const paragraph of paragraphs) {
    paragraph.style.backgroundColor = "#ff0000";
}

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.getElementById("optionsList");

console.log(optionsList);
console.log(optionsList.parentElement);

for (const node of optionsList.childNodes) {

    console.log(node);
    console.log("Type of node: " + node.nodeType);

}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let testParagraph = document.getElementById("testParagraph");

testParagraph.innerHTML = "<p>This is a paragraph</p>";

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let mainHeaderElements = document.querySelector(".main-header").children;

console.log(mainHeaderElements);

for (const mainHeaderElement of mainHeaderElements) {
    mainHeaderElement.classList.add("nav-item");
}

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitles = document.getElementsByClassName("section-title");

while (sectionTitles.length != 0) {
    sectionTitles[0].classList.remove("section-title");
}





